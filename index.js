// EXPRESS SETUP
// 1. Import by using the 'require' directive
const express = require('express')
// 2. Use the express() function and assign it to the app variable
const app = express() 
// 3. Declare a variable for the port of this server
const port = 3000

// These two .use is essential in express
// 4. Use the middleware to enable our server to read JSON as regular Javascript
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// 5. You can then have your routes after all that.

// ROUTES
// Get request route
app.get('/', (req, res) =>{
	res.send('Hello World!')
})

// Post request route
app.post('/hello', (req, res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})
// Register user route
let users = [];
app.post('/register', (req, res) => {
	console.log(req.body)
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		console.log(users)
		res.send(`User ${req.body.username} successfully registered!`)
	} else {
		res.send('Please input BOTH username and password.')
	}
})

// Put request route
// Loop through the whole users array (declared above) so that the server can check if the username that was inputted from the request matches any of the existing usernames inside that array
app.put('/change-password', (req, res) => {
	// Declare a message var that will be re-assigned a string depending on the outcome of the process.
	let message 

	for(let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username){ //IF a username matches, re-assign/change the existing user's password
			users[i].password == req.body.password
			message = `User ${req.body.username}'s password has been updated!`
			break
		} else { // If no user matches, set the message var to a string saying that the user does not exist.
			message = `User does not exist.`
		}
	} // Send a response with the respective message depending on the result of the condition
	res.send(message)
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))